source "azure-arm" "example" {
    tenant_id                         = var.tenant_id
    subscription_id                   = var.subscription_id

    managed_image_resource_group_name = var.resource_group_name
    managed_image_name                = var.image_name

    # az vm image list --publisher=RedHat --all --sku=8.2 --offer=RHEL
    os_type                           = "Linux"
    image_publisher                   = "RedHat"
    image_offer                       = "RHEL"
    image_sku                         = "8.2"
    image_version                     = "8.2.2021040911"

    location                          = "West Europe"
    vm_size                           = "Standard_DS2_v2"

    azure_tags                        = {}
}

build {
    sources = [
        "source.azure-arm.example"
    ]

    provisioner "shell" {
        scripts = ["./scripts/install_nginx.sh"]
    }
}
