#!/bin/bash

sudo dnf -y install nginx
sudo systemctl enable --now nginx
sudo firewall-cmd --permanent --add-port=80/tcp
sudo firewall-cmd --reload
