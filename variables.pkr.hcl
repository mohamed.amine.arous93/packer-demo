variable "resource_group_name" {
    description = "managed image resource group name"
    default     = "demo-packer-rg"
}

variable "image_name" {
    description = "managed image resource group name"
    default     = "demo-image"
}

variable "tenant_id" {
    description = "azure tenant ID"
    default     = "a40b3811-f11b-4cc3-ad26-625a59e97c5b" # az account show --query "tenantId"
}

variable "subscription_id" {
    description = "azure subscription ID"
    default     = "8c66d8ea-389d-4f1c-8c71-a9bff7faf8dd" # az account show --query "id"
}
